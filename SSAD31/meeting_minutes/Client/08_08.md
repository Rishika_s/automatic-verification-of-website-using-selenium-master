#                                                                       SSAD 31

#                                                           Automatic Verification Of Web 


### Client Mentor:- Siva Gaggara                                                                                   
### Organization:- OnwardHealth, CIE

#                                                              Minutes Of Meeting
#                                                  Meeting-1-Introductory meeting with client

***Date***:-    8 August,2017

***Timing***:-5-6 pm

***Venue***:-CIE(Vindhya C4, IIIT-H) 

***Attendees***:-Sri Harsha Vuyyuri, Karthika Ramineni, Rishika Sharma 

***Headed by***:- Sri Harsha Vuyyuri, Karthika Ramineni, Rishika Sharma 

***Agenda:-***
This was the introductory meet with our client regarding a brief description about *selenium*.

* Discussed about the brief introduction to *onward-health* site.
* Description about the working of selenium and the languages which it used .
* Had a look for model specification and look for thebrief  techincal part of work of *selenium* with python.

