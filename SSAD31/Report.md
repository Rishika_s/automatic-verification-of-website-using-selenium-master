### Automated Verification of Onward Health website using Selenium

* Features verified:-
  
  * add hospital
  * add staff
  * search
  * pagination 
  * change password 
  * filter
  * Edit details
  * view messages
  * manage staff(activate,deactivate,grant access,revoke access etc)
  * send messages, notifications
  * add videos, articles
  * View list of items displayed like list of patients, staff
  * discharge patients, logout etc

* Bugs detected :-

  * Deactivate Feature is not working properly.
  * For example when super admin deactivates a call center staff,still call center staff is able to login.