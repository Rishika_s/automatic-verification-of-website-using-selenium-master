# Automated Verification Of Web Applications

### Description

Our Project is *AUTOMATION OF WEB APPLICATIONS VERIFICATION*.Specifically for OnwardHealth which is a Post-Surgery digital 
companion for a patient,We would be working towards automating verification of most of the web interfaces supported by 
the platform. That is, whenever a new feature is being deployed into the production, we can check its working and the 
integrity of the web application by running this automation script.

Complete testing of a web-based system before going live can help address issues before the system is revealed to the public.
Issues such as the security of the web application, the basic functionality of the site, its ability to adapt to the multitude
of desktops, devices, and operating systems, as well as readiness for expected traffic and number of users and the ability to 
survive a massive spike in user traffic, both of which are related to load testing.
         
### Advantages Of Automation 
* It allows one to deploy new features rapidly into production by checking  the working of the new features along with 
  already existing features.
* It also helps to maintain the application quality with minimal human resources.

### Technology used in our project
*selenium* is used in automation of web applications.

> Selenium is software testing framework for web applications. 

It provides a way to automate web application verification. Selenium supports several languages(like python, java, PHP etc).
We are using *selenium with python*.

This includes Writing a Automation Script for populating various fields in the DataBase tables and also writing automation 
script for retrieving data from various fieds of the database tables and validating with the original data.This also includes 
checking various elements of the webpage and the information displayed on various platforms of their website.
